﻿using CarParkRateCalculationApp.Model;
using System;
using System.Collections.Generic;

namespace CarParkRateCalculationApp.BA
{
    public interface IParkingRateEngine
    {
        ParkingRate CalculateParkingRates(DateTime enterDateTime, DateTime exitDateTime, List<ParkingRate> rates);
    }
}
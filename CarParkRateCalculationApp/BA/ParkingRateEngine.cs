﻿using CarParkRateCalculationApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarParkRateCalculationApp.BA
{
    public class ParkingRateEngine : IParkingRateEngine
    {
        /// <summary>
        /// Calcuate Parking Rates based on Enter and Exit timings
        /// </summary>
        /// <param name="enterDateTime">Enter Time</param>
        /// <param name="exitDateTime">Exit Time</param>
        /// <param name="rates"></param>
        /// <returns></returns>
        public ParkingRate CalculateParkingRates(DateTime enterDateTime, DateTime exitDateTime, List<ParkingRate> rates)
        {
            var calculatedParkingRates = rates.Where
                (r =>
                    //Validating Week Days 
                    ((r.EnterFrom.TimeOfDay <= enterDateTime.TimeOfDay && enterDateTime.TimeOfDay <= r.EnterTo.TimeOfDay) &&
                    (r.ExitFrom.TimeOfDay <= exitDateTime.TimeOfDay && exitDateTime.TimeOfDay <= r.ExitTo.TimeOfDay) &&
                    (r.EnterFrom.Date.DayOfWeek != DayOfWeek.Sunday && r.EnterFrom.Date.DayOfWeek != DayOfWeek.Saturday) &&
                    (enterDateTime.DayOfWeek != DayOfWeek.Saturday && enterDateTime.DayOfWeek != DayOfWeek.Sunday))
                     ||
                    //Validating Weekends
                    ((r.EnterFrom.Date.DayOfWeek == DayOfWeek.Saturday && r.EnterTo.Date.DayOfWeek == DayOfWeek.Sunday) &&
                    (r.ExitFrom.Date.DayOfWeek == DayOfWeek.Saturday && r.ExitTo.Date.DayOfWeek == DayOfWeek.Sunday) &&
                    (enterDateTime.DayOfWeek == DayOfWeek.Saturday || enterDateTime.DayOfWeek == DayOfWeek.Sunday) &&
                    (exitDateTime.DayOfWeek == DayOfWeek.Saturday || exitDateTime.DayOfWeek == DayOfWeek.Sunday))
                  ).FirstOrDefault();

            // if no deal then standard rates will be applied 
            if (calculatedParkingRates == null)
            {
                var standardRates = rates.Where(r => r.Name == "Standard Rate").FirstOrDefault();

                var parkingDuration = (exitDateTime - enterDateTime).TotalMinutes;
                // checking if duration is 1 hour
                if (Math.Round(parkingDuration) <= 60)
                {
                    var hourlyRate = standardRates.HourlyRates.Where(r => r.Hours == 1).FirstOrDefault();
                    calculatedParkingRates = standardRates;
                    calculatedParkingRates.Price = hourlyRate.Rate;
                }
                // checking if duration is 2 hours
                if (Math.Round(parkingDuration) <= 120)
                {
                    var hourlyRate = standardRates.HourlyRates.Where(r => r.Hours == 2).FirstOrDefault();
                    calculatedParkingRates = standardRates;
                    calculatedParkingRates.Price = hourlyRate.Rate;
                }
                // checking if duration is 3 hours
                if (Math.Round(parkingDuration) <= 180)
                {
                    var hourlyRate = standardRates.HourlyRates.Where(r => r.Hours == 3).FirstOrDefault();
                    calculatedParkingRates = standardRates;
                    calculatedParkingRates.Price = hourlyRate.Rate;
                }
                // checking if duration is more than 3 hours
                if (Math.Round(parkingDuration) > 180)
                {
                    var hourlyRate = standardRates.HourlyRates.Where(r => r.Hours == 4).FirstOrDefault();
                    calculatedParkingRates = standardRates;
                    calculatedParkingRates.Price = hourlyRate.Rate;
                }

            }

            return calculatedParkingRates;
        }
    }
}

﻿using CarParkRateCalculationApp.Model;
using System.Collections.Generic;

namespace CarParkRateCalculationApp.Data
{
    public interface IParkingRatesData
    {
        List<ParkingRate> GetParkingRates();
    }
}
﻿using CarParkRateCalculationApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarParkRateCalculationApp.Data
{
    public class ParkingRatesData : IParkingRatesData
    {
        public List<ParkingRate> GetParkingRates()
        {
            return new List<ParkingRate> {
                 new ParkingRate{
                    Name = "Early Bird",
                    Type = "Flat Rate",
                    Price = 13,
                    EnterFrom = new DateTime(1900, 01, 01, 6, 00, 00),
                    EnterTo = new DateTime(1900, 01, 01, 9, 00, 00),
                    ExitFrom = new DateTime(1900, 01, 01, 15, 30, 00),
                    ExitTo = new DateTime(1900, 01, 01, 23, 30, 00),
                },
                 new ParkingRate{
                    Name = "Night Rate",
                    Type = "Flat Rate",
                    Price = 6.50m,
                    EnterFrom = new DateTime(1900, 01, 01, 18, 00, 00),
                    EnterTo = new DateTime(1900, 01, 01, 23, 59, 00),
                    ExitFrom = new DateTime(1900, 01, 01, 15, 30, 00),
                    ExitTo = new DateTime(1900, 01, 01, 23, 30, 00),
                },
                new ParkingRate{
                    Name = "Weekend Rate",
                    Type = "Flat Rate",
                    Price = 10,
                    EnterFrom = new DateTime(1900, 01, 06, 00, 00, 00),
                    EnterTo = new DateTime(1900, 01, 07, 23, 59, 00),
                    ExitFrom = new DateTime(1900, 01, 06, 00, 00, 00),
                    ExitTo = new DateTime(1900, 01, 07, 23, 59, 00),
                }
                ,
                 new ParkingRate{
                    Name = "Standard Rate",
                    Type = "Flat Rate",
                    Price = 0,
                    EnterFrom = new DateTime(1900, 01, 01, 00, 00, 00),
                    EnterTo = new DateTime(1900, 01, 01, 00, 00, 00),
                    ExitFrom = new DateTime(1900, 01, 01, 00, 00, 00),
                    ExitTo = new DateTime(1900, 01, 01, 00, 00, 00),

                    HourlyRates=new List<HourlyRate>
                    {
                        new HourlyRate
                        {
                             Hours=1,
                             Rate=5
                        },
                        new HourlyRate
                        {
                             Hours=2,
                             Rate=10
                        }
                        ,
                        new HourlyRate
                        {
                             Hours=3,
                             Rate=15
                        },
                         new HourlyRate
                        {
                             Hours=4,
                             Rate=20
                        }
                     }
                 }
            };
        }

    }
}

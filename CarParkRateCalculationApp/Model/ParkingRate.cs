﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarParkRateCalculationApp.Model
{
    public class ParkingRate
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public decimal Price { get; set; }
        public DateTime EnterFrom { get; set; }
        public DateTime EnterTo { get; set; }
        public DateTime ExitFrom { get; set; }
        public DateTime ExitTo { get; set; }

        public List<HourlyRate> HourlyRates { get; set; }
    }

    public class HourlyRate
    {
        public int Hours { get; set; }
        public decimal Rate { get; set; }
    }

}

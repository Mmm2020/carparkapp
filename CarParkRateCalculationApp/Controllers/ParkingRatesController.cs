﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarParkRateCalculationApp.BA;
using CarParkRateCalculationApp.Data;
using CarParkRateCalculationApp.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CarParkRateCalculationApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ParkingRatesController : ControllerBase
    {
        private IParkingRatesData _IParkingRatesData;
        private IParkingRateEngine _IParkingRateEngine;

        public ParkingRatesController(IParkingRatesData iParkingRatesData, IParkingRateEngine iParkingRateEngine)
        {
            
            _IParkingRatesData = iParkingRatesData;
            _IParkingRateEngine = iParkingRateEngine;
        }
        /// <summary>
        /// API to calculate Parking rates based on entry and exit
        /// https://localhost:44380/parkingrates?enterDateTime=2020-07-03T08:30:00&exitDateTime=2020-07-03T15:30:00
        /// https://localhost:44380/parkingrates?enterDateTime=2020-07-03T18:30:00&exitDateTime=2020-07-03T15:30:00
        /// 
        /// https://localhost:44380/parkingrates?enterDateTime=2020-07-03T18:30:00&exitDateTime=2020-07-03T15:30:00 test it
        /// </summary>
        /// <param name="enterDateTime"></param>
        /// <param name="exitDateTime"></param>
        /// <returns></returns>
        [HttpGet]
        public ParkingRate Get(DateTime enterDateTime, DateTime exitDateTime)
        {
            var parkingRates = _IParkingRatesData.GetParkingRates();
            var calculatedParkingRate = _IParkingRateEngine.CalculateParkingRates(enterDateTime, exitDateTime, parkingRates);
            return calculatedParkingRate;
        }
    }
}

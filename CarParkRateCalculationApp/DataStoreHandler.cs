﻿using CarParkRateCalculationApp.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarParkRateCalculationApp
{
    public class DataStoreHandler
    {
        public List<ParkingRate> ReadDate()
        {
            var parkingRates = new List<ParkingRate>();
            var filePath = AppDomain.CurrentDomain.BaseDirectory + "VehcileDataFile.json";

            //If no vehilce info saved and no file created
            if (System.IO.File.Exists(filePath))
            {
                var jsonString = System.IO.File.ReadAllText(filePath);

                if (jsonString.Length > 0)
                {
                    try
                    {
                        parkingRates = JsonConvert.DeserializeObject<List<ParkingRate>>(jsonString);
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            return parkingRates;
        }

        public void WriteData(List<ParkingRate> parkingRates)
        {
            var filePath = AppDomain.CurrentDomain.BaseDirectory + "ParkingRates.json";

            var jsonValue = JsonConvert.SerializeObject(parkingRates);

            System.IO.File.WriteAllText(filePath, jsonValue.ToString());

        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CarParkRateCalculationApp;
using System;
using System.Collections.Generic;
using System.Text;
using CarParkRateCalculationApp.Model;
using System.Linq;
using CarParkRateCalculationApp.Data;
using CarParkRateCalculationApp.BA;

namespace CarParkRateCalculationApp.Tests
{
    [TestClass()]
    public class ParkingRateCalcTests
    {
        ParkingRatesData _ParkingRatesData;
        ParkingRateEngine _ParkingRateEngine;

        [TestInitialize()]
        public void init()
        {
            _ParkingRatesData = new ParkingRatesData();
            _ParkingRateEngine = new ParkingRateEngine();
        }

        
        /// <summary>
        /// Entry Condition	Enter between 6:00 AM to 9:00 AM
        /// Exit Condition  Exit between 3:30 PM to 11:30 PM

        /// </summary>
        [TestMethod()]
        public void TestEarlyBirdRates()
        {

            var parkingRates = _ParkingRatesData.GetParkingRates();
           
            var enterDateTime = new DateTime(2020, 07, 3,8,30,10);
            var exitDateTime = new DateTime(2020, 07, 3, 15, 31, 10);

            var enterFrom = parkingRates.FirstOrDefault().EnterFrom;
            var enterTo = parkingRates.FirstOrDefault().EnterTo;

            var weStart = parkingRates.FirstOrDefault().EnterFrom.DayOfWeek;
            var weEnd = parkingRates.FirstOrDefault().EnterTo.DayOfWeek;

            var calculatedParkingRates = _ParkingRateEngine.CalculateParkingRates(enterDateTime, exitDateTime, parkingRates);

            bool isPassed = false;
            if (calculatedParkingRates!=null)
            {
                isPassed = calculatedParkingRates.Name == "Early Bird";
            }

            Assert.IsTrue(isPassed);
        }
        /// <summary>
        /// Entry Condition	Enter between 6:00 PM to midnight (weekdays)
        /// Exit Condition  Exit between 3:30 PM to 11:30 PM
        /// </summary>
        [TestMethod()]
        public void TestNightRates()
        {
            var parkingRates = _ParkingRatesData.GetParkingRates();
            var enterDateTime = new DateTime(2020, 07, 1, 23, 01, 10);
            var exitDateTime = new DateTime(2020, 07, 2, 15, 31, 10);

            var calculatedParkingRates = _ParkingRateEngine.CalculateParkingRates(enterDateTime, exitDateTime, parkingRates);

            bool isPassed = false;
            if (calculatedParkingRates!=null)
            {
                isPassed = calculatedParkingRates.Name == "Night Rate";
            }

            Assert.IsTrue(isPassed);
        }

        [TestMethod()]
        public void TestNightRatesBeforeWeekendStart()
        {
            var parkingRates = _ParkingRatesData.GetParkingRates();
            var enterDateTime = new DateTime(2020, 07, 3, 23, 58, 10);
            var exitDateTime = new DateTime(2020, 07, 4, 08, 31, 10);
                     
            var calculatedParkingRates = _ParkingRateEngine.CalculateParkingRates(enterDateTime, exitDateTime, parkingRates);

            bool isPassed = false;
            if (calculatedParkingRates != null)
            {
                isPassed = calculatedParkingRates.Name == "Night Rate";
            }
            //This should fail as it doest not quality for night rate because exiting before 3:30 pm
            Assert.IsFalse(isPassed);
        }
        /// <summary>
        /// Entry Condition	Enter anytime past midnight on Friday to Sunday
        /// Exit Condition  Exit any time before midnight of Sunday
        /// </summary>
        [TestMethod()]
        public void TestWeekendRates()
        {
            var parkingRates = _ParkingRatesData.GetParkingRates();
           
            var enterDateTime = new DateTime(2020, 07, 4, 8, 01, 10);
            var exitDateTime = new DateTime(2020, 07, 4, 15, 31, 10);

            var calculatedParkingRates = _ParkingRateEngine.CalculateParkingRates(enterDateTime, exitDateTime, parkingRates);

            bool isPassed = false;
            if (calculatedParkingRates!=null)
            {
                isPassed = calculatedParkingRates.Name == "Weekend Rate";
            }

            Assert.IsTrue(isPassed);
        }

        [TestMethod()]
        public void TestStandardRates()
        {

            var parkingRates = _ParkingRatesData.GetParkingRates();
            var enterDateTime = new DateTime(2020, 07, 3, 10, 01, 10);
            var exitDateTime = new DateTime(2020, 07, 3, 12, 31, 10);

            var calculatedParkingRates = _ParkingRateEngine.CalculateParkingRates(enterDateTime, exitDateTime, parkingRates);

            bool isPassed = false;
            if (calculatedParkingRates != null)
            {
                if(calculatedParkingRates.Name == "Standard Rate")
                {
                   isPassed = true;
                }
            }

            Assert.IsTrue(isPassed);
        }

        [TestMethod()]
        public void TestStandardRatesWithOneHour()
        {
            var parkingRates = _ParkingRatesData.GetParkingRates();
            var enterDateTime = new DateTime(2020, 07, 3, 10, 40, 10);
            var exitDateTime = new DateTime(2020, 07, 3, 11, 31, 10);

            var calculatedParkingRates = _ParkingRateEngine.CalculateParkingRates(enterDateTime, exitDateTime, parkingRates);

            bool isPassed = false;
            if (calculatedParkingRates != null)
            {
                if (calculatedParkingRates.Name == "Standard Rate")
                {
                    double parkingDuration = (exitDateTime - enterDateTime).TotalMinutes;

                    if(Math.Round(parkingDuration)<=60)
                    {
                        var rates =calculatedParkingRates.HourlyRates.Where(r => r.Hours == 1).FirstOrDefault();

                        isPassed = true;
                    }

                    
                }
            }

            Assert.IsTrue(isPassed);
        }

        [TestMethod()]
        public void TestStandardRatesWithTwoHours()
        {
            var parkingRates = _ParkingRatesData.GetParkingRates();
            var enterDateTime = new DateTime(2020, 07, 3, 10, 40, 10);
            var exitDateTime = new DateTime(2020, 07, 3, 11, 50, 10);
          
            var calculatedParkingRates = _ParkingRateEngine.CalculateParkingRates(enterDateTime, exitDateTime, parkingRates);

            bool isPassed = false;
            if (calculatedParkingRates != null)
            {
                if (calculatedParkingRates.Name == "Standard Rate")
                {
                    var parkingDuration = (exitDateTime - enterDateTime).TotalMinutes;

                    if (Math.Round(parkingDuration) <= 120)
                    {
                        var rates = calculatedParkingRates.HourlyRates.Where(r => r.Hours == 2).FirstOrDefault();

                        isPassed = true;
                    }
                }
            }

            Assert.IsTrue(isPassed);
        }


        [TestMethod()]
        public void TestStandardRatesWithThreeHours()
        {
            var parkingRates = _ParkingRatesData.GetParkingRates();

            var enterDateTime = new DateTime(2020, 07, 3, 10, 40, 10);
            var exitDateTime = new DateTime(2020, 07, 3, 13, 31, 10);
 
            var calculatedParkingRates = _ParkingRateEngine.CalculateParkingRates(enterDateTime, exitDateTime, parkingRates);

            bool isPassed = false;
            if (calculatedParkingRates != null)
            {
                if (calculatedParkingRates.Name == "Standard Rate")
                {
                    var parkingDuration = (exitDateTime - enterDateTime).TotalMinutes;

                    if (Math.Round(parkingDuration) <= 180)
                    {
                        var rates = calculatedParkingRates.HourlyRates.Where(r => r.Hours == 3).FirstOrDefault();

                        isPassed = true;
                    }
                }
            }

            Assert.IsTrue(isPassed);
        }

        [TestMethod()]
        public void TestStandardRatesWithThreeHoursPlus()
        {
            var parkingRates = _ParkingRatesData.GetParkingRates();
            var enterDateTime = new DateTime(2020, 07, 3, 10, 40, 10);
            var exitDateTime = new DateTime(2020, 07, 3, 14, 31, 10);

            var calculatedParkingRates = _ParkingRateEngine.CalculateParkingRates(enterDateTime, exitDateTime, parkingRates);

            bool isPassed = false;
            if (calculatedParkingRates!=null)
            {
                if (calculatedParkingRates.Name == "Standard Rate")
                {
                    var parkingDuration = (exitDateTime - enterDateTime).TotalMinutes;

                    if (Math.Round(parkingDuration) > 180)
                    {
                        var rates = calculatedParkingRates.HourlyRates.Where(r => r.Hours == 4).FirstOrDefault();

                        isPassed = true;
                    }
                }
            }

            Assert.IsTrue(isPassed);
        }

    }
}